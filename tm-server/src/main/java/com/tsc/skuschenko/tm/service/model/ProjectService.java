package com.tsc.skuschenko.tm.service.model;

import com.tsc.skuschenko.tm.api.repository.model.IProjectRepository;
import com.tsc.skuschenko.tm.api.service.model.IProjectService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.repository.model.ProjectRepository;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractBusinessService<Project>
        implements IProjectService {

    @NotNull
    @Override
    @SneakyThrows
    public Project add(
            @Nullable final User user, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);
        @NotNull final IProjectRepository projectRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@Nullable final List<Project> projects) {
        Optional.ofNullable(projects).ifPresent(
                items -> items.forEach(
                        item -> add(
                                item.getUser(),
                                item.getName(),
                                item.getDescription())
                )
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status.getDisplayName());
            project.setStatus(status.getDisplayName());
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status.getDisplayName());
            project.setStatus(status.getDisplayName());
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status.getDisplayName());
            project.setStatus(status.getDisplayName());
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;

        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        try {
            @NotNull final IProjectRepository projectRepository =
                    context.getBean(ProjectRepository.class,entityManager);
            projectRepository.clearAllProjects();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            entityRepository.clear(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;

        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project completeByName(@NotNull final String userId,
                                  @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            return entityRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<Project> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {

            return entityRepository.findAllWithUserId(userId)
                    .stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            return entityRepository.findAllWithUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            return entityRepository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            return entityRepository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            return entityRepository.findOneByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(entityRepository.getReference(id))
                            .orElseThrow(ProjectNotFoundException::new);
            entityRepository.remove(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(ProjectNotFoundException::new);
            entityRepository.remove(
                    entityRepository.getReference(project.getId())
            );
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(ProjectNotFoundException::new);
            entityRepository.remove(
                    entityRepository.getReference(project.getId())
            );
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        @NotNull final Project project = Optional.ofNullable(
                findOneById(userId, id)
        ).orElseThrow(ProjectNotFoundException::new);
        project.setDateFinish(new Date());
        project.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project = Optional.ofNullable(
                    findOneByIndex(userId, index)
            ).orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project = Optional.ofNullable(
                    findOneByName(userId, name)
            ).orElseThrow(ProjectNotFoundException::new);
            project.setDateFinish(new Date());
            project.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project = Optional.ofNullable(
                    findOneById(userId, id)
            ).orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IProjectRepository entityRepository =
                context.getBean(ProjectRepository.class,entityManager);
        try {
            @NotNull final Project project = Optional.ofNullable(
                    findOneByIndex(userId, index)
            ).orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            entityRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
