package com.tsc.skuschenko.tm.exception.entity.user;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
