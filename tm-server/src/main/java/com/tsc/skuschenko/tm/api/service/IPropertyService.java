package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.other.ISaltSetting;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthor();

    @NotNull
    String getAuthorEmail();

    @NotNull
    Integer getBackupTime();

    @Nullable String getFactoryClass();

    @Nullable
    String getFileBackupPath();

    @Nullable
    String getFileBase64Path();

    @Nullable
    String getFileBinaryPath();

    @Nullable
    String getFileJsonPath(@Nullable String className);

    @NotNull
    Integer getFileScannerTime();

    @Nullable
    String getFileXmlPath(@Nullable String className);

    @Nullable
    String getFileYamlPath();

    @Nullable String getJdbcDialect();

    @Nullable
    String getJdbcDriver();

    @Nullable String getJdbcHbm2ddl();

    @Nullable
    String getJdbcPassword();

    @Nullable String getJdbcShowSql();

    @Nullable
    String getJdbcUrl();

    @Nullable
    String getJdbcUserName();

    @Nullable String getLiteMember();

    @Nullable String getMinimalPuts();

    @Nullable String getProviderConfiguration();

    @Nullable String getQueryCache();

    @Nullable String getRegionPrefix();

    @Nullable String getSecondLevel();

    @Nullable
    String getServerHost();

    @Nullable
    String getServerPort();

    @Nullable
    String getSessionCycle();

    @Nullable
    String getSessionSalt();

}
