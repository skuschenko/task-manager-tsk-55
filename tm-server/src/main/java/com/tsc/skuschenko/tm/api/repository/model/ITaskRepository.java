package com.tsc.skuschenko.tm.api.repository.model;

import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IAbstractRepository<Task> {

    void clear(@NotNull String userId);

    void clearAllTasks();

    @Nullable List<Task> findAll();

    @Nullable
    List<Task> findAllTaskByProjectId(
            @NotNull String userId, @NotNull String projectId
    );

    @Nullable List<Task> findAllWithUserId(@NotNull String userId);

    @Nullable
    Task findById(@NotNull String id);

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable Task getReference(@NotNull String id);

    void removeOneById(@NotNull String userId, @NotNull String taskId);

}
