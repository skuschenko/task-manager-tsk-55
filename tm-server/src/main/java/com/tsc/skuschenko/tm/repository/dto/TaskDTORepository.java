package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.api.repository.dto.ITaskDTORepository;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskDTORepository extends AbstractDTORepository<TaskDTO>
        implements ITaskDTORepository {

    public TaskDTORepository(@NotNull @Autowired EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String query =
                "DELETE FROM TaskDTO e WHERE e.userId = :userId";
        entityManager.createQuery(query)
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clearAllTasks() {
        @NotNull final String query = "DELETE FROM TaskDTO e";
        entityManager.createQuery(query).executeUpdate();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final String query = "SELECT e FROM TaskDTO e ";
        return entityManager.createQuery(query, TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAllTaskByProjectId(
            @NotNull String userId, @NotNull String projectId
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDTO e WHERE e.userId = :userId AND " +
                        "e.projectId = :projectId";
        return entityManager.createQuery(query, TaskDTO.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllWithUserId(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM TaskDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(query, TaskDTO.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findById(@NotNull String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String query =
                "SELECT e FROM TaskDTO e WHERE e.userId = :userId AND " +
                        "e.id = :id";
        TypedQuery<TaskDTO> typedQuery =
                entityManager.createQuery(query, TaskDTO.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(
            @NotNull String userId, @NotNull Integer index
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDTO e WHERE e.userId = :userId";
        TypedQuery<TaskDTO> typedQuery =
                entityManager.createQuery(query, TaskDTO.class)
                        .setParameter("userId", userId).
                        setFirstResult(index);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public TaskDTO findOneByName(
            @NotNull String userId, @NotNull String name
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDTO e WHERE e.userId = :userId AND " +
                        "e.name = :name";
        TypedQuery<TaskDTO> typedQuery =
                entityManager.createQuery(query, TaskDTO.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Override
    public void removeOneById(
            @NotNull String userId, @NotNull String taskId
    ) {
        @NotNull final String query =
                "DELETE FROM TaskDTO e WHERE e.userId = :userId AND " +
                        "e.id = :id";
        entityManager.createQuery(query)
                .setParameter("userId", userId)
                .setParameter("id", taskId).executeUpdate();
    }

}