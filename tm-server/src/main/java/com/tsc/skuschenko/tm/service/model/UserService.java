package com.tsc.skuschenko.tm.service.model;

import com.tsc.skuschenko.tm.api.repository.model.IUserRepository;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.model.IUserService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.*;
import com.tsc.skuschenko.tm.exception.entity.user.EmailExistsException;
import com.tsc.skuschenko.tm.exception.entity.user.LoginExistsException;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.repository.model.UserRepository;
import com.tsc.skuschenko.tm.util.HashUtil;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public final class UserService extends AbstractBusinessService<User>
        implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    public void addAll(@Nullable final List<User> users) {
        Optional.ofNullable(users).ifPresent(
                items -> items.forEach(
                        item -> create(
                                item.getLogin(),
                                item.getPasswordHash(),
                                item.getEmail()
                        )
                )
        );
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        try {
            @NotNull final IUserRepository userRepository =
                    context.getBean(UserRepository.class,entityManager);
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(secret, iteration, password));
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        if (isEmailExist(email)) throw new EmailExistsException();
        if (isLoginExist(login)) throw new LoginExistsException();
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(secret, iteration, password));
            user.setEmail(email);
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final Role role
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(secret, iteration, password));
            user.setEmail(role.getDisplayName());
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository entityRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            return entityRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyEmailException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            return userRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            return userRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (!Optional.ofNullable(email).isPresent()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (!Optional.ofNullable(login).isPresent()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            @NotNull final User user = Optional.ofNullable(findByLogin(login))
                    .orElseThrow(UserNotFoundException::new);
            user.setLocked(true);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            return userRepository.removeByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration
                = propertyService.getPasswordIteration();
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            @NotNull final User user = Optional.ofNullable(findById(userId))
                    .orElseThrow(UserNotFoundException::new);
            user.setPasswordHash(HashUtil.salt(secret, iteration, password));
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            @NotNull final User user = Optional.ofNullable(findByLogin(login))
                    .orElseThrow(UserNotFoundException::new);
            user.setLocked(false);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String userId, @Nullable final String firstName,
            @Nullable final String lastName, @Nullable final String middleName
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final IUserRepository userRepository =
                context.getBean(UserRepository.class,entityManager);
        try {
            @NotNull final User user = Optional.ofNullable(findById(userId))
                    .orElseThrow(UserNotFoundException::new);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}