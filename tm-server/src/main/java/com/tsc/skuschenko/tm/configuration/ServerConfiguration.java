package com.tsc.skuschenko.tm.configuration;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@ComponentScan("com.tsc.skuschenko.tm")
public class ServerConfiguration {

    @NotNull
    @Bean
    @Scope("prototype")
    public EntityManager entityManager(
            @NotNull @Autowired final EntityManagerFactory entityManagerFactory
    ) {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    @Bean
    public EntityManagerFactory entityManagerFactory(
            @NotNull @Autowired final IConnectionService connectionService
    ) {
        return connectionService.getHibernateSqlFactory();
    }

}
