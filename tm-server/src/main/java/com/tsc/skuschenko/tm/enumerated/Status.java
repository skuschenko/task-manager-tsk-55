package com.tsc.skuschenko.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum Status {

    NOT_STARTED("not started"),
    IN_PROGRESS("in progress"),
    COMPLETE("complete");

    @NotNull
    private final String displayName;

}
