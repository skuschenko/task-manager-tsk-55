package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.api.repository.model.IAbstractRepository;
import com.tsc.skuschenko.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity>
        implements IAbstractRepository<E> {

    @NotNull
    protected  EntityManager entityManager;

    @Override
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    @Nullable
    public E getEntity(@NotNull TypedQuery<E> query) {
        @NotNull final List<E> projectDTOList = query.getResultList();
        if (projectDTOList.isEmpty()) return null;
        return projectDTOList.get(0);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entityManager.remove(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

}
