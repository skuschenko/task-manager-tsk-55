package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.api.service.ICommandService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Component
public final class HelpShowCommand extends AbstractCommand {

    private static final String ARGUMENT = "-h";

    private static final String DESCRIPTION = "help";

    private static final String NAME = "help";
    @Autowired
    private ICommandService commandService;

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @NotNull final Collection<AbstractCommand> commands =
                commandService.getCommands();
        commands.stream().filter(item -> Optional.ofNullable(item).isPresent())
                .forEach(item -> {
                    String result = "";
                    if (Optional.ofNullable(item.name()).isPresent()) {
                        result += item.name();
                    }
                    if (Optional.ofNullable(item.arg()).isPresent()) {
                        result += " [" + item.arg() + "]";
                    }
                    if (Optional.ofNullable(item.description()).isPresent()) {
                        result += " - " + item.description();
                    }
                    System.out.println(result);
                });
    }

    @Override
    public String name() {
        return NAME;
    }

}
