package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.SessionEndpoint;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    private static final String DESCRIPTION = "logout current user";

    private static final String NAME = "logout";

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        sessionEndpoint.closeSession(session);
    }

    @Override
    public String name() {
        return NAME;
    }

}
