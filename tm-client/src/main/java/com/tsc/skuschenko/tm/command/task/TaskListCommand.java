package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.endpoint.TaskEndpoint;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public final class TaskListCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "show all tasks";

    private static final String NAME = "task-list";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("sort");
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> tasks = new ArrayList<>();
        if (sort.isEmpty()) {
            tasks = taskEndpoint.findTaskAll(session);
        } else {
            tasks = taskEndpoint
                    .findTaskAllSort(session, sort);
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public String name() {
        return NAME;
    }

}
