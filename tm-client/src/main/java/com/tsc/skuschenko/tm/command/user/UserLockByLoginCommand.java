package com.tsc.skuschenko.tm.command.user;


import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.UserEndpoint;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class UserLockByLoginCommand extends AbstractUserCommand {

    private static final String DESCRIPTION = "lock user by login";

    private static final String NAME = "lock-user-by-login";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @NotNull final String login = TerminalUtil.nextLine();
        userEndpoint.lockUserByLogin(session, login);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String[] roles() {
        return new String[]{"Administrator"};
    }

}
