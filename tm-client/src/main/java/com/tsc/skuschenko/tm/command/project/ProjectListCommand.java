package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Project;
import com.tsc.skuschenko.tm.endpoint.ProjectEndpoint;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public final class ProjectListCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "show all projects";

    private static final String NAME = "project-list";
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("sort");
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Project> projects = new ArrayList<>();

        if (sort.isEmpty()) {
            projects = projectEndpoint
                    .findProjectAll(session);
        } else {
            projects = projectEndpoint
                    .findProjectAllSort(session, sort);
        }
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @Override
    public String name() {
        return NAME;
    }

}
