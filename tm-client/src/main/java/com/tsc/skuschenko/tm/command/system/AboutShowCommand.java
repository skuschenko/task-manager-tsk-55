package com.tsc.skuschenko.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class AboutShowCommand extends AbstractCommand {

    private static final String ARGUMENT = "-a";

    private static final String DESCRIPTION = "about";

    private static final String NAME = "about";

    @Autowired
    private ISessionService sessionService;

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        System.out.println("AUTHOR: " + Manifests.read("developer"));
        System.out.println("EMAIL: " + Manifests.read("email"));
    }

    @Override
    public String name() {
        return NAME;
    }

}
