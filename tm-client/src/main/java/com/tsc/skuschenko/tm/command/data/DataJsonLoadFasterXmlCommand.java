package com.tsc.skuschenko.tm.command.data;


import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.DataEndpoint;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    private static final String DESCRIPTION = "load data from json file";

    private static final String NAME = "data-load-json";

    @Autowired
    private DataEndpoint dataEndpoint;

    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        dataEndpoint.loadDataJsonFasterXmlCommand(session);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String[] roles() {
        return new String[]{"Administrator"};
    }

}
