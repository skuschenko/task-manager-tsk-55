package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Project;
import com.tsc.skuschenko.tm.endpoint.ProjectEndpoint;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "update project by index";

    private static final String NAME = "project-update-by-index";
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private ISessionService sessionService;

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("index");
        @NotNull final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        Project project = projectEndpoint
                .findProjectByIndex(session, valueIndex);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showParameterInfo("name");
        @NotNull final String valueName = TerminalUtil.nextLine();
        showParameterInfo("description");
        @NotNull final String valueDescription = TerminalUtil.nextLine();
        project = projectEndpoint.updateProjectByIndex(
                session, valueIndex, valueName, valueDescription
        );
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    @Override
    public String name() {
        return NAME;
    }

}
